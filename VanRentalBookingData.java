import java.util.Calendar;

	/**
	 * VanRentalBookingData contains the vital information of each VanRentalBooking. 
	 * That is, it contains the ID, start date and end dates of bookings made.
	 * 
	 * @author eunikeutojo
	 * 
	*/
public class VanRentalBookingData implements Comparable <VanRentalBookingData>{
	private String id; //ID of the van rental booking
	private Calendar startHireDate; //the start hire date of the booking
	private Calendar endHireDate; //the end hire date of the booking
	
	/** 
	 * Creates a new VanRentalBookingData object with the desired ID, start date and end date.
	 * 
	 * @pre input start date < inpuat end date
	 * @param id - the booking id to be set
	 * @param start - the start date of booking
	 * @param end - the end date of the booking
	 * @post true
	 * @returns n/a
	*/
	public VanRentalBookingData(String id, Calendar start, Calendar end){
		this.id = id;
		this.startHireDate = start;
		this.endHireDate = end;
	}
	
	/**
	 * Used when changing a requested booking, method replaces the ID of an existing booking with a new one.
	 * 
	 * @pre id is a valid id
	 * @param id - new id of the booking to be altered
	 * @post id is changed to the new id
	 * @return n/a
	 */
	
	public void changeId(String id){
		this.id = id;
	}
	
	/**
	 * Gets the booking ID of an existing booking
	 * 
	 * @pre booking is valid
	 * @param n/a
	 * @post true
	 * @return String - containing the booking ID
	*/
	public String getBookingId(){
		return this.id;
	}
	
	/**
	 * Gets the start hire date of an existing booking
	 * 
	 * @pre booking is valid
	 * @param n/a
	 * @post true
	 * @return Calendar - start date of the booking
	*/
	public Calendar getStartHireDate(){
		return this.startHireDate;
	}
	
	/**
	 * Gets the end hire date of an existing booking
	 * 
	 * @pre true
	 * @param n/a
	 * @post true
	 * @return Calendar - end date of the booking
	*/
	public Calendar getEndHireDate(){
		return this.endHireDate;
	}
	/**
	 * This override is used to sort the bookings assoicated with a van by date.
	 * 
	 * @pre booking to be compared is valid
	 * @param b - the booking to be compared to
	 * @post list is now in ascending order
	 * @return sorts the bookings by ascending date
	 */
	@Override
	public int compareTo(VanRentalBookingData b) {
		return getStartHireDate().compareTo(b.getStartHireDate());
	}
	
}
