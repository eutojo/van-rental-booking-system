import java.util.ArrayList;

	/**
	 * This class contains all the details regarding each rental van.
	 * This includes the name and type.
	 * 
	 * It also holds an array list which contains the bookings associated with each van.
	 * 
	 * @author eunikeutojo
	 *
	 */
public class RentalVan {

	private String RentalVan;
	private String VanType;
	private ArrayList<VanRentalBookingData> VanRentalBookingsList = new ArrayList<VanRentalBookingData>();
	
	/**
	 * Creates a new RentalVan with a set name and type.
	 * 
	 * @pre valid van name and type
	 * @param van
	 * @param type
	 * @post true
	 * @return n/a
	*/
	public RentalVan(String van, String type){
		this.RentalVan = van;
		this.VanType = type;
	}
	
	/**
	 * Gets the name of an existing van
	 * 
	 * @pre true
	 * @param n/a
	 * @post true
	 * @return String - containing the name of the van
	*/
	public String getVanName(){
		return this.RentalVan;
	}
	
	/**
	 * Gets the type of an existing van
	 * 
	 * @pre true
	 * @param n/a
	 * @post true
	 * @return String - containing the type of the van
	*/
	public String getVanType(){
		return this.VanType;
	}
	
	/**
	 * Gets the bookings associated with an existing van
	 * 
	 * @pre true
	 * @param n/a
	 * @post true
	 * @return ArrayList - containing the bookings of the van
	*/
	public ArrayList<VanRentalBookingData> getBookings(){
		return this.VanRentalBookingsList;
	}
	
	/**
	 * Adds a booking to an existing van
	 * 
	 * @pre the new booking is valid
	 * @param b- the booking to be added to the van
	 * @post true
	 * @return n/a
	*/
	public void addBooking(VanRentalBookingData b){
		this.VanRentalBookingsList.add(b);
	}
	
	/**
	 * Removes an existing booking
	 * 
	 * @pre the booking to be deleted is valid
	 * @param b - the booking to be deleted from the van
	 * @post true
	 * @return n/a
	*/
	public void removeBooking(VanRentalBookingData b){
		this.VanRentalBookingsList.remove(b);
	}
}
