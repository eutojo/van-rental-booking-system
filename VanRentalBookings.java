import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;

	/**
	 * The VanRentalBookings class deals with the outputs for the different input cases
	 * Request - Attempts to make a booking given a time period and desired van types
	 * Cancel - Attempts to cancel a request based on an ID provided
	 * Change - Attempts to change a request based on an ID provided
	 * Print - Prints a list of rented vans and their hire times based on a depot provided
	 * 
	 * It also contains an array list containing the name of the vans rented for each input to enable
	 * easier print functions.
	 * 
	 * @author eunikeutojo
	 * 
	*/
public class VanRentalBookings {

	private ArrayList<String> RentedVanList = new ArrayList<String>(); //to store the name of the rented vans
	
	public String output; //used in all cases

	private Calendar startHireDate; //used to set the start and end dates of REQUEST and CHANGE
	private Calendar endHireDate;  //similar to above
	private String id; // used to set the booking ID REQUEST and CHANGE
	private int availability; //used to make a booking to ensure that only available vans are assigned 
	private int makeBookingChange; //for changing bookings
	private int deleted; //to ensure that bookings are properly deleted
	private String tempId; //for changing bookings, tempID = -1
	private String originalId; //for changing bookings, the change the booking ID from the tempId of -1 to the original ID
	
	/** 
	 * Attempts to fulfill a given REQUEST.
	 * First it checks to see if a valid ID is requested, then checks to see if there are enough vans available.
	 * @pre the given input is in a certain order as outlined by the assessment sheet
	 * @param VanRentalDepotList - list of rental vans 
	 * @param arguments - lines scanned from the input text
	 * @post only vans available are assigned 
	 * @return String- List of vans hired to be printed via the VanRentalSystem class
	*/
	public String bookingRequest(ArrayList<VanRentalDepot> VanRentalDepotList, String...splitLine){
		
		this.id = splitLine[1];
		int hour1 = Integer.parseInt(splitLine[2]);
		String month1 = splitLine[3];
		int date1 = Integer.parseInt(splitLine[4]);
		int hour2 = Integer.parseInt(splitLine[5]);
		String month2 = splitLine[6];
		int date2 = Integer.parseInt(splitLine[7]);
		int num1 = Integer.parseInt(splitLine[8]);
		String type1 = splitLine[9];
		
		this.startHireDate = new GregorianCalendar(); // set the dates required for the booking
		this.endHireDate = new GregorianCalendar();	
		setDate(this.startHireDate, hour1, month1, date1);
		setDate(this.endHireDate, hour2, month2, date2);
		
		int noAuto = 0;  // initialise the number of automatic and manual vans requested
		int noManual = 0;
		if(type1.equals("Automatic")){	//assign the number of automatic and manual vans
			noAuto = num1;
		} else {
			noManual = num1;
		}
		try{
			try {
				int num2 = Integer.parseInt(splitLine[10]);	//try to see if there is more than one type requested
				String type2 = splitLine[11];
				if(type2.equals("Automatic")){
					noAuto = num2;
				} else {
					noManual = num2;
				}
			} catch (NumberFormatException e){
				
			}
		} catch(IndexOutOfBoundsException e){
			
		}
		
		this.makeBookingChange = 0; //not making a booking change so set this to 0
		
		if(checkBookings(VanRentalDepotList, this.id)==true){ //check if ID has been used already
			output = "Booking rejected";
		} else if(makeBooking(VanRentalDepotList, noAuto, noManual)==true){ //if not, try to make a booking
			 output = "Booking " +this.id + " " ;
			for(VanRentalDepot d: VanRentalDepotList){
				int firstDepot = 0;
				for(RentalVan v: d.getVanList()){
					for(String name: RentedVanList){
						if(v.getVanName().equals(name)){ // compare the RentedVanList with the vans a
							if(firstDepot == 0){		//	a depot
								output = output + d.getVanRentalDepot() + " " + name;
								firstDepot = 1;
							} else {
								output = output + name ;
							}
							output = output + ", ";
						}
					}
				}
				if(firstDepot == 1) {
					output = output.substring(0, output.length()-2); // delete the comma and replace
					output = output + "; "; 						// with semicolon
				}
			}				
			output = output.substring(0, output.length()-2);	//delete the last semi colon
		} else {
			removeBooking(VanRentalDepotList, this.id);	//if booking not possible, delete the failed booking
			output = "Booking rejected";
		}
		
		return output;
	}
	
	/** 
	 * Attempts to make a booking
	 * First it makes a new booking
	 * Then checks to see if it is a valid booking
	 * If the desired timeslot is taken, delete the booking
	 * 
	 * @pre noAuto >= 0 && noManual >= 0
	 * @param VanRentalDepotList - list of rental vans
	 * @param noAuto - number of automatics required
	 * @param noManual - number of manuals required
	 * @post bookings are only made if there is the correct number of required vans
	 * @return boolean - true if possible, false if not possible
	*/
	public boolean makeBooking(ArrayList<VanRentalDepot> VanRentalDepotList, int noAuto, int noManual){
			
		if(this.makeBookingChange == 1){	//if making a change, set the ID to -1
			this.id = this.tempId;
		}
				
		VanRentalBookingData newBooking = new VanRentalBookingData(this.id, this.startHireDate, this.endHireDate);

		this.RentedVanList.clear();	//clear the rented van list for every new line scanned

			for(VanRentalDepot d: VanRentalDepotList){	
				for(RentalVan v: d.getVanList()){
					this.availability = 1;
					
					//checking for automatic
					if(v.getVanType().equals("Automatic") && (noAuto != 0)){ //skip if required number is found
						for(VanRentalBookingData b: v.getBookings()){
							if(makeBookingChange ==1 && b.getBookingId().equals(this.originalId)){	//skip the availability check if
							} else {																//changing the booking so that
								availabilityCheck(newBooking, b);									//the vans previously booked isn't
							}																		//marked as unavailable
							if(this.availability==0){ // not available
								break;
							}
						}
						if(this.availability == 1){
							v.addBooking(newBooking);
							this.RentedVanList.add(v.getVanName());
							noAuto--;
						}
					//checking for manual, repeat the same process as automatic
					} else if(v.getVanType().equals("Manual") && (noManual != 0)){
						for(VanRentalBookingData b: v.getBookings()){
							if(makeBookingChange ==1 && b.getBookingId().equals(this.originalId)){
								
							} else {
								availabilityCheck(newBooking, b);
							}
							if(this.availability==0){ // not available
								break;
							}
						}
						if(this.availability == 1){
							v.addBooking(newBooking);
							this.RentedVanList.add(v.getVanName());
							noManual--;
						}
					
					}
				}
			}
			
			this.makeBookingChange = 0;		//reset the booking change
			
			if(noAuto!= 0 || noManual != 0){	//if not enough vans assigned, the makeBooking has failed
				return false;
			} else {
				return true;
			}					
		}
	
	/** 
	 * Attempts to CHANGE a given booking.
	 * First, it attempts to make a booking through makeBooking
	 * Similar to REQUEST, but this bypasses the ID check so that the vans hired through the previous
	 * REQUEST is not taken into account.
	 * This is to ensure that the required number of vans are available for the CHANGE.
	 * If available, cancel the old booking and make a new one.
	 * 
	 * @pre bookings can only be changed if it already exists
	 * @param VanRentalDepotList - list of rental vans
	 * @param ...splitLine - lines scanned from the input text
	 * @post CHANGE only possible if enough vans are available
	 * @return String - Results of the CHANGE attempt
	*/
	public String bookingChange(ArrayList<VanRentalDepot> VanRentalDepotList, String...splitLine){
		//the start is similar to REQUEST
		this.id = splitLine[1];
		int hour1 = Integer.parseInt(splitLine[2]);
		String month1 = splitLine[3];
		int date1 = Integer.parseInt(splitLine[4]);
		int hour2 = Integer.parseInt(splitLine[5]);
		String month2 = splitLine[6];
		int date2 = Integer.parseInt(splitLine[7]);
		int num1 = Integer.parseInt(splitLine[8]);
		String type1 = splitLine[9];
		
		this.startHireDate = new GregorianCalendar();
		this.endHireDate = new GregorianCalendar();
		setDate(this.startHireDate, hour1, month1, date1);
		setDate(this.endHireDate, hour2, month2, date2);
		
		this.makeBookingChange = 1;
		this.tempId = "-1"; //temp ID is set to -1
		this.originalId = this.id;
		output = "";
		int noAuto = 0;
		int noManual = 0;
		
		if(type1.equals("Automatic")){
			noAuto = num1;
		} else {
			noManual = num1;
		}
		try{
			try {
				int num2 = Integer.parseInt(splitLine[10]);
				String type2 = splitLine[11];
				if(type2.equals("Automatic")){
					noAuto = num2;
				} else {
					noManual = num2;
				}
			} catch (NumberFormatException e){
				
			}
		} catch(IndexOutOfBoundsException e){

		}	
		
		if(checkBookings(VanRentalDepotList, this.id)==false){
			output = "Change rejected";
		} else if (makeBooking(VanRentalDepotList, noAuto, noManual)==true){
			removeBooking(VanRentalDepotList, this.originalId);
				 output = "Change " + this.originalId + " " ;
				 for(VanRentalDepot d: VanRentalDepotList){
						int firstDepot = 0;
						for(RentalVan v: d.getVanList()){
							for(String name: RentedVanList){
								if(v.getVanName().equals(name)){ // compare the RentedVanList with the vans a
									if(firstDepot == 0){		//	a depot
										output += d.getVanRentalDepot() + " " + name;
										firstDepot = 1;
									} else {
										output += name ;
									}
									output += ", ";
								}
							}
							for(VanRentalBookingData b: v.getBookings()){
								if(b.getBookingId().equals(this.id)){
									b.changeId(this.originalId);
								}
							}
						}
						if(firstDepot ==1) {
							output = output.substring(0, output.length()-2); // delete the comma and replace
							output += "; "; 						// with semicolon
						}		
					}
					output = output.substring(0, output.length()-2);
			} else {
				removeBooking(VanRentalDepotList, this.tempId);
				output = "Change rejected";
			}
		return output;
	}
	
	/**
	 * Attempts to CANCEL a given booking
	 * First checks to see if booking exists
	 * @pre valid ID is given
	 * @param VanRentalDepotList
	 * @param inputID
	 * @post cancelled only if booking exists
	 * @return String
	*/
	public String bookingCancel(ArrayList<VanRentalDepot> VanRentalDepotList, String inputId){	
		if(checkBookings(VanRentalDepotList, inputId) == false){
			output = "Cancel rejected";
		}
		
		removeBooking(VanRentalDepotList, inputId);
		
		if(this.deleted == 1){ //if 1, then the vans were able to be unassigned, if 0 then no vans were unassigned
			output = "Cancel " + inputId;
		} else {
			output = "Cancel rejected";
		}
		return output;
	}
	
	/**
	 * Attempts to remove bookings
	 * 
	 * @pre valid ID provided
	 * @param VanRentalDepotList - list of rental vans
	 * @param inputId - the id of the booking to be removed
	 * @post only deleted if ID exists
	 * @return n/a
	 */
	public void removeBooking(ArrayList<VanRentalDepot> VanRentalDepotList, String inputId){
		
		this.deleted  = 0;
		
		for(int i = 0; i < VanRentalDepotList.size (); i++){ //iterate through array
			VanRentalDepot d = VanRentalDepotList.get(i);
			for(int j = 0; j < d.getVanList().size(); j++){
				RentalVan v = d.getVanList().get(j);
				for(int k = 0; k < v.getBookings().size(); k++){
					VanRentalBookingData b = v.getBookings().get(k);
					if(b.getBookingId().equals(inputId)){
						v.removeBooking(b);
						this.deleted = 1; //if it reaches here, the vans were able to be unassigned, and booking deleted
					}
					
				}
			}
		}
	}
	
	
	/** 
	 * PRINT the bookings associated with the vans at a given depot
	 *  
	 * @pre valid depot provided
	 * @param VanRentalDepotList - list of rental vans
	 * @param depot - name of the depot to be scanned
	 * @post only van bookings associated with the depot is printed
	 * @return String- printed list of bookings in order of when it was made
	*/
	public String bookingPrint(ArrayList<VanRentalDepot> VanRentalDepotList, String depot){
			output = "" ; //reset the string
			for(VanRentalDepot d: VanRentalDepotList){
				if(d.getVanRentalDepot().equals(depot)){
					for(RentalVan v: d.getVanList()){
						Collections.sort(v.getBookings());
						for(VanRentalBookingData b: v.getBookings()){
							String start = format(b.getStartHireDate());
							String end = format(b.getEndHireDate());
							output += d.getVanRentalDepot() +" "+ v.getVanName() +" "+ start + " " + end + "\n";
							}
					}
				}
			}	
			
			return output;
	}
	
	/**
	 * Checks if desired time slot is free, taking into account the 1 buffering cooling period
	 * Creates a new Calendar and copies data onto it in order to create the 1 hour buffer to
	 * compare
	 * 
	 * @pre valid bookings provided
	 * @param requestedBooking - booking to be made
	 * @param oldBooking - bookings already made
	 * @post correctly sets the times
	 * @return n/a
	*/
	public void availabilityCheck(VanRentalBookingData requestedBooking, VanRentalBookingData oldBooking){
		
		//create new calendar to enable easier comparison
		Calendar freeBefore = new GregorianCalendar();
		Calendar freeAfter = new GregorianCalendar();
		//adding 1 hour each way to create the buffer
		freeBefore = oldBooking.getStartHireDate();
		freeBefore.add(Calendar.HOUR, -1);
		freeAfter = oldBooking.getEndHireDate();
		freeAfter.add(Calendar.HOUR, 1);
				
		//has to either start and end 1 hour before a taken slot
		//or start 1 hour after taken slot
		//if not satisfied, not available
		if(!((requestedBooking.getStartHireDate().before(freeBefore) && requestedBooking.getEndHireDate().before(freeBefore)) ||
				(requestedBooking.getStartHireDate().after(freeAfter) && requestedBooking.getEndHireDate().after(freeAfter)))){
			this.availability = 0;
		}
		
		//add the hour back
		freeBefore.add(Calendar.HOUR, 1);
		freeAfter.add(Calendar.HOUR, -1);

	}
	
	/**
	 * Checks if a booking already exists by checking the ID requested to the booking IDs
	 * 
	 * @pre valid id
	 * @param VanRentalDepotList - list of rental vans
	 * @param id - id of the requested booking to be checked
	 * @post n/a
	 * @return boolean - true if already exists, false if doesn't
	 */
	public boolean checkBookings(ArrayList<VanRentalDepot> VanRentalDepotList, String id){
		for(VanRentalDepot d: VanRentalDepotList){
			for(RentalVan v: d.getVanList()){
				for(VanRentalBookingData b: v.getBookings()){
					if(b.getBookingId().equals(id)){	//if there's a match, the booking already exists
						return true;
					}
				}
			}
		}	
		return false;
	}
	

	/**
	 * Sets the date of requested booking
	 * 
	 * @pre hour is in 24 hour time, month is a string of the first three letters of a valid month
	 * @pre day is a valid number in the month
	 * @param date - startHireDate or endHireDate to be set
	 * @param hour - hour requested
	 * @param month - month requested
	 * @param day - day requested
	 * @post date set correctly
	 * @return void
	*/
	public void setDate(Calendar date, int hour, String month, int day){
		int intMonth = 0;	
		switch(month){
			case "Jan":
				intMonth = 0; //january starts at 0
				break;
			case "Feb":
				intMonth = 1;
				break;
			case "Mar":
				intMonth = 2;
				break;
			case "Apr":
				intMonth = 3;
				break;
			case "May":
				intMonth = 4;
				break;
			case "Jun":
				intMonth = 5;
				break;
			case "Jul":
				intMonth = 6;
				break;
			case "Aug":
				intMonth = 7;
				break;
			case "Sep":
				intMonth = 8;
				break;
			case "Oct":
				intMonth = 9;
				break;
			case "Nov":
				intMonth = 10;
				break;
			case "Dec":
				intMonth = 11;
				break;
		}
		
		date.set(Calendar.YEAR, 2017); //set the year to 2017
		date.set(Calendar.MONTH, intMonth);
		date.set(Calendar.HOUR_OF_DAY, hour);
		date.set(Calendar.DAY_OF_MONTH, day);
		date.set(Calendar.MINUTE, 00); //set the minutes to 0
		
	}
	
	/**
	 * This sets the date in the required format as required of the assignment
	 * 
	 * @pre input date is valid
	 * @param date - date to be formatted
	 * @post date formatted correctly
	 * @return String of the date in the form HH:mm MMM dd
	*/
	public static String format(Calendar date){
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm MMM dd"); //format as indicated by the specs
		sdf.setCalendar(date);
		String dateFormatted = sdf.format(date.getTime());
		return dateFormatted;
	}
	
	
}