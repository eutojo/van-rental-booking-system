import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * The main class for the back end of the van rental system.
 * Scans in an input from a text file and provides output accordingly, depending ont the
 * scanned line.
 * 
 * It scans inputs by turning the scanned line into an array based on the spaces it scans.
 * 
 * It also allocates vans accordingly by first checking if the desired depot already exists.
 * If not, it will create a new depot.
 * 
 * @author eunikeutojo
 *
 *@pre the scanned text is not a single symbol
 *@param args[] - arguments from command line
 *@post information returned is correct
 *@return String- details of information requested
 *
 */
public class VanRentalSystem {
	
	public static void main(String args[]){
		
	    Scanner sc = null;
	    String line;
	    
	    VanRentalDepot newDepot;
	    RentalVan newVan;
	    
	    ArrayList<VanRentalDepot> VanRentalDepotList = new ArrayList<VanRentalDepot>(); //list of depots
	    VanRentalBookings b = new VanRentalBookings();	//new booking instruction
	    
	    try
	    {
	        	sc = new Scanner(new FileReader(args[0]));
	        
		        while(sc.hasNextLine()){
	  	        	line = sc.nextLine();
	  	        	String[] splitLine = line.split("\\W+");
	  	    
	  	        	switch(splitLine[0]){
	  	        		case "Location":
	  	        		    boolean retval = false;
	  	        			
	  	        			newDepot = new VanRentalDepot(splitLine[1]);
	  	  	        		newVan = new RentalVan(splitLine[2], splitLine[3]);
	
	
	  	        			for(VanRentalDepot d : VanRentalDepotList ){
	  	        				if(newDepot.getVanRentalDepot().equals(d.getVanRentalDepot())){ //if name depot exists, just add van
	  	        					d.addRentalVan(newVan);
	  	        					retval = true;
	  	        				}
	  	        			}
	  	        			  if(retval == false){	//if not, add new depot and add van to new depot;
	  	        				VanRentalDepotList.add(newDepot);
	  	        		  		newDepot.addRentalVan(newVan);
	  	        			  } 			    	        			  
		        			break;
		        		case "Request":
		        			System.out.println(b.bookingRequest(VanRentalDepotList, splitLine));
		        			break;
		        		case "Change":
		        			System.out.println(b.bookingChange(VanRentalDepotList, splitLine));
		        			break;	
		        		case "Cancel":
		        			System.out.println(b.bookingCancel(VanRentalDepotList, splitLine[1]));
		        			break;
		        		case "Print":
		        			System.out.print(b.bookingPrint(VanRentalDepotList, splitLine[1]));
		        			break;
		        		default:	//for comments
		        			break;
		        		
		        	}
		        }
	        
	    }
	    
	    catch (FileNotFoundException e) {}
	    
	    finally
	    {
	        if (sc != null) sc.close();
	    }
	    
	}
}