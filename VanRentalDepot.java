import java.util.ArrayList;

/**
 * VanRentalDepot contains information about the different depots.
 * It also contains an arraylist of the vans detained in the depots
 * 
 * @author eunikeutojo
 * 
 */

public class VanRentalDepot {
	
	private String VanRentalDepot;
	private ArrayList<RentalVan> RentalVanList = new ArrayList<RentalVan>();

	/**
	 * Creates a new VanRentalDepot object and sets the name
	 * 
	 * @pre valid depot name
	 * @param d - the name of the new van rental depot
	 * @post van rental depot name set
	 * @return n/a
	*/
	public VanRentalDepot(String d){
		this.VanRentalDepot = d;
	}
	
	/**
	 * Gets the name of the VanRentalDepot
	 * @pre true
	 * @param n/a
	 * @post true
	 * @return String - name of the depot
	*/
	public String getVanRentalDepot(){
		return this.VanRentalDepot;
	}
	
	/**
	 * Adds van to the depot
	 * 
	 * @pre valid van 
	 * @param v name of the rental van to be added to the depot
	 * @post van added to existing depot
	 * @return n/a
	*/
	public void addRentalVan(RentalVan v){
		this.RentalVanList.add(v);
	}
	
	/**
	 * Returns 
	 * @pre depot must be valid
	 * @param n/a
	 * @post true
	 * @return ArrayList - containing the vans at the depot
	*/
	public ArrayList<RentalVan> getVanList(){
		return this.RentalVanList;
	}
	
}
